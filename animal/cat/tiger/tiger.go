package tiger

import "gitlab.com/ari-salt/zoo/animal/cat"

type tiger struct {
	cat.Cat
}

func (t *tiger) Rawr() string {
	return "Rawr!"
}

func New(name, kind string) *tiger {
	c := cat.New(name, kind)

	return &tiger{
		Cat: *c,
	}
}
